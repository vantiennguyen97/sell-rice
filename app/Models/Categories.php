<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Dimsav\Translatable\Translatable;

class Categories extends Model implements Transformable
{
    use HasFactory;
    use TransformableTrait;
    use SoftDeletes;
    
    protected $fillable = [
        'name',
        'description',
        'image',
        'branch_id',
        'font_weight',
        'font_text',
        'font_size',
        'font_style',
        'user_id',
    ];
    protected $date = ['deleted_at'];
    public function user()
    {
        $this->belongsTo(User::class);
    }
}
