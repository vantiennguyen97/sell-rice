<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use PhpOffice\PhpSpreadsheet\Calculation\Category;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'price',
        'small_image',
        'brand_id',
        'category_id',
        'extra_limit',
        'quantity_of_day',
        'quantity_active',
        'sale_off',
        'sale_off_holiday',
        'sale_off_monday',
        'sale_off_tuesday',
        'sale_off_wednesday',
        'sale_off_thursday',
        'sale_off_friday',
        'sale_off_saturday',
        'created_by',
        'updated_by',
    ];
    protected $date =['delete_at'];

    public function createdByUser()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedByUser()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
