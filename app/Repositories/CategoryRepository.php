<?php
namespace App\Repositories;

use App\Models\Categories;
use Whoops\Run;

class CategoryRepository implements CategoryRepositoryInterface
{
    protected $model;
    
    public function __construct(Categories $model)
    {
        $this->model = $model;
    }
    public function all()
    {
        return $this->model->all();
    }
    public function findId($id)
    {
        return $this->model::find($id);
    }
    public function create(array $data)
    {
        return $this->model->create($data);
    }
    public function update($id, array $data)
    {
        $record = $this->model::find($id);
        // if($record['name'] == $data['name']){
        //     $message = 'name has been existed !!';
        //     return $message;
        // }
        return $record->update($data);
    }
    public function delete($id)
    {
        return $this->model->destroy($id);
    }
    public function forceDelete($id){
        $category = Categories::withTrashed()->findOrFail($id);
        return $category->forceDelete();
    }
    public function restore($id){
        $category = Categories::withTrashed()->findOrFail($id);
        return  $category->restore();
    }
    public function deleteAllRecord()
    {
        return $this->model::query()->delete();
    }
    public function import(array $data){
        $category = $this->model->import($data);
        return $category;
    }
}