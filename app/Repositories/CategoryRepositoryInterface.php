<?php
namespace App\Repositories;

interface CategoryRepositoryInterface
{
    public function all();
    public function findId($id);
    public function create(array $data);
    public function update($id, array $data);
    public function delete($id);
    public function forceDelete($id);
    public function restore($id);
    public function deleteAllRecord();
    public function import(array $data);
}