<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Services\Admin\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(UserService $userService){
        $this->userService = $userService;
    }

    public function index(Request $request){
        $data = $this->userService->index($request);
        return view('admin.user.index')->with(['data' => $data]);
    }

    public function create(){
        $users = new User();
        return view('admin.user.create')->with(['users' => $users]);
    }

    public function store(UserRequest $request){
        $data = $this->userService->store($request);
        Session::flash('msg_pusher_success', $data['message']);
        return redirect('admin/users')->with(['message' => $data['message'], 'alert-class' => 'alert-success']);

    }

    public function edit($id) {
        $users = $this->userService->edit($id);
        if(!$users){
            abort(404);
        }
        return view('admin.user.edit')->with(['users' => $users]);
    }

    public function update(UserRequest $request) {
        $data = $this->userService->update($request);
        Session::flash('msg_pusher_success', $data['message']);
        return redirect('admin/users')->with(['message' => $data['message'], 'alert-class' => 'alert-success']);
    }

    public function destroy(Request $request){
        $data = $this->userService->destroy($request);
        Session::flash('msg_pusher_success', $data['message']);
        return redirect('admin/users')->with(['message' => $data['message'], 'alert-class' => 'alert-success']);
    }
}
