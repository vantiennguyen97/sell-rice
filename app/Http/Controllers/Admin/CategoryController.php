<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categories;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Nette\Utils\Image;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rules\Exists;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    private $font_text = array([
        'Arial' => 'Arial',
        'Times New Roman' => 'Times New Roman',
        'Helvetica' => 'Helvetica',
        'Times' => 'Times',
        'Courier New' => 'Courier New',
        'Verdana' => 'Verdana',
        'Courier' => 'Courier',
        'Arial Narrow' => 'Arial Narrow',
        'Candara' => 'Candara',
        'Geneva' => 'Geneva',
        'Calibri' => 'Calibri',
        'Optima' => 'Optima',
        'Cambria' => 'Cambria',
        'Garamond' => 'Garamond',
        'Perpetua' => 'Perpetua',
        'Monaco' => 'Monaco',
        'Didot' => 'Didot',
        'Brush Script MT' => 'Brush Script MT',
        'Lucida Bright' => 'Lucida Bright',
        'Copperplate' => 'Copperplate'
    ]);
    private $font_weight = array([
        'Normal' => 'Normal',
        'Bold' => 'Bold',
    ]);
    private $font_style = array([
        'Normal' => 'Normal',
        'Italic' => 'Italic',
        'Oblique' => 'Oblique',
    ]);
    protected $categoryRepository;


    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $listCategory = $this->categoryRepository->all();

        return view('admin.categories.index', compact('listCategory'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $font_text = $this->font_text;
        $font_weight = $this->font_weight;
        $font_style = $this->font_style;
        return view('admin.categories.create', compact('font_text', 'font_weight', 'font_style'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['branch_id'] = 1;
        $data['user_id'] = 1;
        if ($request->file('image')) {
            $image = $this->saveImageCategory($request);
            $data['image'] = $image;
        }

        $request->validate([
            'name' => 'required|unique:categories,name',
        ], [
            'name.required' => 'You must fill name for category.',
            'name.unique' => 'The name has already been taken.'
        ]);
        $category = $this->categoryRepository->create($data);
        $response = [
            'message' => __('Create successfully !!'),
            'data' => $category->toArray(),
        ];
        if ($request->wantsJson()) {

            return response()->json($response);
        }

        return redirect()->route('admin.listCategory')->with(['message' => $response['message'], 'alert-class' => 'alert-success']);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->findId($id);
        $imageName = $category->image;
        $font_text = $this->font_text;
        $font_weight = $this->font_weight;
        $font_style = $this->font_style;
        if (!$category) {
            abort(403);
        }
        // dd('hello');
        return view('admin.categories.edit', compact('category', 'imageName', 'font_text', 'font_weight', 'font_style'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['branch_id'] = 1;
        $data['user_id'] = 1;
        if ($request->file('image')) {
            $image = $this->saveImageCategory($request);
            $data['image'] = $image;
        }
        $updatedCategory = $this->categoryRepository->update($id, $data);
        $response = [
            'message' => __('Update succesfully !!'),
            // 'data' => $updatedCategory->toArray(),
        ];
        if ($request->wantsJson()) {

            return response()->json($response);
        }
        return redirect()->route('admin.listCategory')->with(['message' => $response['message'], 'alert-class' => 'alert-success']);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $data = $this->categoryRepository->delete($id);
        $response = [
            'message' => __('Delete successfully !!'),
        ];
        return redirect()->route('admin.listCategory')->with(['message' => $response['message'], 'alert-class' => 'alert-success']);
    }

    public function forceDelete($id)
    {
        $category = $this->categoryRepository->forceDelete($id);
        return redirect()->route('admin.listCategory')->with('message', 'Deleted data from DB');
    }

    public function destroyAll()
    {
        $categories = $this->categoryRepository->deleteAllRecord();
        return redirect()->route('admin.listCategory')->with('message', 'All record are deleted !!');
    }

    private function saveImageCategory(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $imageName = time() . '.' . $request->image->extension();

        $imagePath = public_path('categories/resize') . $imageName;

        if (File::exists($imagePath)) {
            File::delete($imagePath);
        }
        $request->image->move(public_path('categories/resize'), $imageName);
        return $imageName;
    }
    public function export()
    {
       $spreadsheet = new Spreadsheet();
       $sheet = $spreadsheet->getActiveSheet();

       $sheet->setCellValue('A1','Excel form');
       $sheet->setCellValue('B2','ID');
       $sheet->setCellValue('C2','Name');
       $sheet->setCellValue('D2','Description');
       $sheet->setCellValue('E2','Font text');
       $sheet->setCellValue('F2','Font size');
       $sheet->setCellValue('G2','Font weight');
       $sheet->setCellValue('H2','Font style');
       $sheet->setCellValue('I2','Image url');
       $sheet->setCellValue('J2','User id');

       $categories = $this->categoryRepository->all();

       $row = 3;
       foreach($categories as $category){
        $sheet->setCellValue('B'.$row,$category->id);
        $sheet->setCellValue('C'.$row,$category->name);
        $sheet->setCellValue('D'.$row,$category->description);
        $sheet->setCellValue('E'.$row,$category->font_text);
        $sheet->setCellValue('F'.$row,$category->font_size);
        $sheet->setCellValue('G'.$row,$category->font_weight);
        $sheet->setCellValue('H'.$row,$category->font_style);
        $sheet->setCellValue('I'.$row,$category->image);
        $sheet->setCellValue('J'.$row,$category->user_id);
        $row++;
       }
       $writer = new Xlsx($spreadsheet);

        $response = new StreamedResponse(function() use ($writer) {
            $writer->save('php://output');
        });

        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="users.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');
        return $response;
    }
    public function import()
    {
        return view('admin.categories.import');
    }
    public function uploadImport(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx,xls,csv'
        ]);

        $file = $request->file('file')->path();
        $spreadsheet = IOFactory::load($file);
        $sheet = $spreadsheet->getActiveSheet();
        $rows = $sheet->toArray();
        

        foreach ($rows as $key => $row) {
            if ($key == 0 || $key == 1) {
                continue;
            }
            $nameExist = Categories::where('name',$row[2])->exists();
            if($nameExist){
                return redirect()->route('admin.listCategory')->with('message','Name is duplicate !!');
                break;
            }
            $this->categoryRepository->create([
                'user_id' => 1,
                'name' => $row[2],
                'description' => $row[3],
                'font_text' => $row[4],
                'font_size' => $row[5],
                'font_weight' => $row[6],
                'font_style' => $row[7],
                'image' => $row[8],

            ]);
        }
        return redirect()->route('admin.listCategory')->with('message','Import Success');
    }
}
