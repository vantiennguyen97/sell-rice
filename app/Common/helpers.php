<?php
//Common helpers


use Aws\Exception\AwsException;
use Aws\S3\S3Client;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;

if (!function_exists('format_date')) {
    /**
     * Format date
     *
     * @param String $date : format
     * @param String $format : type format
     *
     * @return String
     * */
    function format_date($date, $format = 'd/m/Y')
    {
        if ($date) {
            return date_format(date_create($date), $format);
        }
        return "";
    }
}
