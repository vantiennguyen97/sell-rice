import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',

                //admin
                'resources/assets/admin/css/app.scss',
                'resources/assets/admin/js/app.js',
            ],
            refresh: true,
        }),
    ],
});
