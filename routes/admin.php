<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Admin\CategoryController;

Route::get('/', [DashboardController::class, 'index'])->name('dashboard');

// Route::resource("categories",CategoryController::class);
Route::get('/categories',[CategoryController::class,'index'])->name('listCategory');
Route::get('/categories/new',[CategoryController::class,'create'])->name('newCategory');
Route::post('/categories/store',[CategoryController::class,'store'])->name('storeCategory');
Route::get('/categories/edit/{id}',[CategoryController::class,'edit'])->name('editCategory');
// Route::get('/categories/edit/{id}', ['uses' => 'CategoriesController@edit', 'permission' => 'category_update']);
Route::post('/categories/update/{id}',[CategoryController::class,'update'])->name('updateCategory');
Route::post('/categories/delete/{id}',[CategoryController::class,'destroy'])->name('deleteCategory');
Route::post('/categories/deleteAll',[CategoryController::class,'destroyAll'])->name('deleteAllRecord');
Route::get('/categories/export',[CategoryController::class,'export'])->name('exportCategory');
// Import
Route::get('/categories/import',[CategoryController::class,'import'])->name('importCategory');
Route::post('/categories/uploadImport',[CategoryController::class,'uploadImport'])->name('uploadImport');
//products
