<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('poducts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable(true);
            $table->float('price');
            $table->string('small_image')->nullable(true);
            $table->integer('extra_limit')->nullable(true);
            $table->integer('quantity_of_day')->nullable(true);
            $table->integer('quantity_active')->nullable(true);
            $table->boolean('sale_off')->nullable(true);
            $table->float('sale_off_holiday')->nullable(true);
            $table->float('sale_off_monday')->nullable(true);
            $table->float('sale_off_tuesday')->nullable(true);
            $table->float('sale_off_wednesday')->nullable(true);
            $table->float('sale_off_thursday')->nullable(true);
            $table->float('sale_off_friday')->nullable(true);
            $table->float('sale_off_saturday')->nullable(true);
            $table->integer('branch_id')->nullable(true);
            $table->foreignId('category_id')->nullable(true)->constrained()->onDelete('cascade');
            $table->integer('created_by')->nullable(true);
            $table->integer('updated_by')->nullable(true);
            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('poducts');
    }
};
