<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable(true);
            $table->string('image')->nullable(true);
            $table->string('font_weight')->nullable(true);
            $table->string('font_text')->nullable(true);
            $table->integer('font_size')->nullable(true);
            $table->string('font_style')->nullable(true);
            $table->integer('branch_id')->nullable(true);
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->softDeletes('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
