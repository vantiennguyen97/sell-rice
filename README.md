
<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

### Support Policy
- Laravel 11.x requires a minimum PHP version of 8.2.
### Use
- Design patent: l5-repository
### Install project
1. run composer
   >composer install
2. copy file .env.example to .env
3. config sql in .env
   ```
   DB_CONNECTION=mysql
   DB_HOST=127.0.0.1
   DB_PORT=
   DB_DATABASE=
   DB_USERNAME=
   DB_PASSWORD=
   ```
4. generate key
    >php artisan key:generate
5. clear cache and config
    > php artisan config:cache
6. run migration
    >php artisan migrate
7. run seeder
    >php artisan db:seed
8. install npm
    >npm i
9. build file js and scss
    >npm run build
10. run server and test
    >php artisan serve

    You also can config hosting in server and run it

