let APP = {};
$(function () {
    'use strict';
    APP.init = function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('.required').append(' <span class="text-danger">*</span>');

        if (msg_pusher_success){
            APP.showMessageSuccess(msg_pusher_success);
        }

        if (msg_pusher_error){
            APP.showMessageError(msg_pusher_error);
        }
    }

    APP.showMessageSuccess = function (message){
        iziToast.success({
            message: message,
            position: 'topRight',
        });
    }

    APP.showMessageError = function (message){
        iziToast.error({
            message: message,
            position: 'topRight',
        });
    }

    APP.showMessageInfo = function (title, message){
        iziToast.info({
            title: title,
            message: message,
            position: 'topRight',
        });
    }

    APP.showMessageWarning = function (title, message){
        iziToast.warning({
            title: title,
            message: message,
            position: 'topRight',
        });
    }

});

$(document).ready(function () {
    APP.init();
});
