<div class="ms-slidebar sb-slidebar sb-left sb-momentum-scrolling sb-style-overlay">
    <header class="ms-slidebar-header">
        <div class="ms-slidebar-title">
            <form class="search-form">
                <input id="search-box-slidebar" type="text" class="search-input" placeholder="Search..." name="q" />
                <label for="search-box-slidebar"><i class="zmdi zmdi-search"></i></label>
            </form>
            <div class="ms-slidebar-t">
                <span class="ms-logo ms-logo-sm">P</span>
                <h3>Profile<span> iSoViet</span></h3>
            </div>
        </div>
    </header>
    <ul class="ms-slidebar-menu" id="slidebar-menu" role="tablist" aria-multiselectable="true">
        <li><a data-scroll class="link" href="#home"><i class="zmdi zmdi-home"></i> Home</a></li>
        <li><a data-scroll class="link" href="#services"><i class="zmdi zmdi-flight-takeoff"></i> Services</a></li>
        <li><a data-scroll class="link" href="#about"><i class="zmdi zmdi-info-outline"></i> About Us</a></li>
        <li><a data-scroll class="link" href="#team"><i class="zmdi zmdi-accounts"></i> Team</a></li>
    </ul>
    <div class="ms-slidebar-social ms-slidebar-block">
        <h4 class="ms-slidebar-block-title">Social Links</h4>
        <div class="ms-slidebar-social">
            <a href="https://www.facebook.com/isoviet.net" target="_blank"
               class="btn-circle btn-circle-raised btn-circle-sm btn-facebook"><i class="zmdi zmdi-facebook"></i></a>
            <a href="https://twitter.com/iso_viet" target="_blank"
               class="btn-circle btn-circle-raised btn-circle-sm btn-twitter"><i class="zmdi zmdi-twitter"></i></a>
        </div>
    </div>
</div>
