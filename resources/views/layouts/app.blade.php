<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#333">
    <title>Profile - iSoViet</title>
    <meta name="description" content="Profile iSoViet">
    <link rel="shortcut icon" href="{{asset('assets/img/favicon.png?v=3')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{asset('assets/css/preload.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/plugins.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.light-blue-500.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <!--[if lt IE 9]>
    <script src="{{asset('assets/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('assets/js/respond.min.js')}}"></script>
    <![endif]-->
</head>

<body>
<div id="ms-preload" class="ms-preload">
    <div id="status">
        <div class="spinner">
            <div class="dot1"></div>
            <div class="dot2"></div>
        </div>
    </div>
</div>
<div class="ms-site-container ms-nav-fixed">
    @include('layouts.navigation')
    <div class="intro-fixed ms-hero-img-keyboard ms-hero-bg-primary color-white" id="home">
        <div class="intro-fixed-content index-1">
            <div class="container">
                <div class="text-center mb-4">
            <span
                class="ms-logo ms-logo-lg ms-logo-white center-block mb-2 mt-2 animated zoomInDown animation-delay-5">P</span>
                    <h1
                        class="no-m ms-site-title color-white center-block ms-site-title-lg mt-2 animated zoomInDown animation-delay-5">
                        Proflie <span class="color-warning">iSoViet</span></h1>
                    <p
                        class="lead lead-lg color-white text-center center-block mt-2 mw-800 text-uppercase fw-300 animated fadeInUp animation-delay-7">
                        We are a team of software developers and testers specializing in developing enterprise applications
                </div>
                <div class="text-center mb-2">
                    <a id="go-intro-fixed-next" href="javascript:void(0)"
                       class="btn-circle btn-circle-raised btn-circle-white animated zoomInUp animation-delay-12"><i
                            class="zmdi zmdi-long-arrow-down"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="btn-back-top">
        <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised "><i
                class="zmdi zmdi-long-arrow-up"></i></a>
    </div>
    <div class="bg-light index-1 intro-fixed-next pt-6" id="intro-next">
        @yield('content')
        @include('layouts.footer')
        <div class="btn-back-top">
            <a href="#" data-scroll id="back-top" class="btn-circle btn-circle-primary btn-circle-sm btn-circle-raised "><i
                    class="zmdi zmdi-long-arrow-up"></i></a>
        </div>
    </div>
</div> <!-- ms-site-container -->
@include('layouts.sidebar')
<!-- Modal -->
<div class="modal" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="projectModalLabel2">
    <div class="modal-dialog modal-lg animated zoomIn animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title color-primary" id="myModalLabel"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i
                            class="zmdi zmdi-close"></i></span></button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>
<script src="{{asset('assets/js/plugins.min.js')}}"></script>
<script src="{{asset('assets/js/app.min.js')}}"></script>
<script src="{{asset('assets/js/lead.js')}}"></script>
<script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>
