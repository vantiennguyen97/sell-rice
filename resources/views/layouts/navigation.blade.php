<nav class="navbar navbar-expand-md navbar-fixed ms-lead-navbar navbar-mode navbar-mode mb-0" id="navbar-lead">
    <div class="container container-full">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">
                <!-- <img src="assets/img/demo/logo-navbar.png" alt=""> -->
                <span class="ms-logo ms-logo-white ms-logo-sm">P</span>
                <span class="ms-title">Profile <strong class="color-warning"> iSoViet</strong></span>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item"><a data-scroll class="nav-link active" href="#home">Home</a></li>
                <li class="nav-item"><a data-scroll class="nav-link" href="#services">Services</a></li>
                <li class="nav-item"><a data-scroll class="nav-link" href="#portfolio">Portfolio</a></li>
                <li class="nav-item"><a data-scroll class="nav-link" href="#about">About us</a></li>
                <li class="nav-item"><a data-scroll class="nav-link" href="#team">Team</a></li>
            </ul>
        </div> <!-- navbar-collapse collapse -->
        <a href="javascript:void(0)" class="ms-toggle-left btn-navbar-menu"><i class="zmdi zmdi-menu"></i></a>
    </div> <!-- container -->
</nav>
