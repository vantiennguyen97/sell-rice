@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit</h1>
        <form action="{{ route('tableTest.update', $tableTest->id) }}" method="POST">
        @csrf
        @method('PUT')
            <button type="submit" class="btn btn-success">Update</button>
        </form>
    </div>
@endsection
