@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Create</h1>
        <form action="{{ route('tableTest.store') }}" method="POST">
        @csrf
            <button type="submit" class="btn btn-success">Create</button>
        </form>
    </div>
@endsection
