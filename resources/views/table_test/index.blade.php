@extends('layouts.app')

@section('content')
    <div class="container">
        <h1> List</h1>
        <a href="{{ route('tableTest.create') }}" class="btn btn-primary">Create New</a>
        <table class="table">
            <thead>
            <tr>
                <!-- Add table headers here -->
            </tr>
            </thead>
            <tbody>
            <!-- Add table data here -->--}}
            </tbody>
        </table>
    </div>
@endsection
