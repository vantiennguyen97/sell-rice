@extends('layouts.app')

@section('content')
    <section id="services" class="">
        <div class="wrap ms-bg-fixed">
            <div class="container index-1">
                <div class="text-center mb-4 mw-800 center-block">
                    <h1 class="text-center wow zoomInDown">Services</h1>
                    <h2 class="text-center color-primary mb-2 wow fadeInDown animation-delay-4">Know our amazing features</h2>
                    <p class="lead text-center aco wow fadeInDown animation-delay-5 mw-800 center-block mb-4">
                        Look great, get noticed, & generate leads
                    </p>
                </div>

                <div class="row">
                    <div class="ms-feature col-xl-4 col-lg-4 col-md-6 col-sm-6 card wow fadeInUp animation-delay-4">
                        <div class="text-center card-body">
                            <span class="ms-icon ms-icon-circle ms-icon-xxlg color-info"><i class="fa fa-at"></i></span>
                            <h4 class="color-info">Web & Mobile Application</h4>
                            <p class="">
                                Create an online presence with a website that's branded with your own logo, photos, content, and
                                more.
                                Development mobile app to support your business.
                            </p>
                        </div>
                    </div>
                    <div class="ms-feature col-xl-4 col-lg-4 col-md-6 col-sm-6 card wow fadeInUp animation-delay-8">
                        <div class="text-center card-body">
                  <span class="ms-icon ms-icon-circle ms-icon-xxlg color-warning"><i
                          class="fa fa-line-chart"></i></span>
                            <h4 class="color-warning">Data Analytics</h4>
                            <p class="">
                                The techniques and processes of data analytics have been automated into mechanical processes and
                                algorithms that work over raw data for human consumption. Data analytics help a business optimize
                                its performance. </p>
                        </div>
                    </div>
                    <div class="ms-feature col-xl-4 col-lg-4 col-md-6 col-sm-6 card wow fadeInUp animation-delay-10">
                        <div class="text-center card-body">
                  <span class="ms-icon ms-icon-circle ms-icon-xxlg color-success"><i
                          class="fa fa-handshake-o"></i></span>
                            <h4 class="color-success">Automation Test</h4>
                            <p class="">
                                Software testing technique to test and compare the actual outcome with the expected outcome. Test
                                automation is used to automate repetitive tasks and other testing tasks which are difficult to
                                perform manually. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio" class="">
        <div class="wrap ms-hero-bg-dark ms-hero-img-keyboard ms-bg-fixed">
            <div class="container index-1">
                <div class="text-center color-white mb-4 mw-800 center-block">
                    <h1 class="wow fadeInUp animation-delay-2">Latest Works</h1>
                    <p class="lead color-medium wow fadeInUp animation-delay-2">Discover our projects and the rigorous process
                        of creation. Our principles are creativity, design, experience and <span
                            class="color-white">knowledge</span>. We are backed by 10 years of research.</p>
                </div>
                <div class="portfolio-items row"></div>
            </div>
        </div>
    </section>
    <section id="about">
        <div class="container pt-6">
            <h2 class="right-line">About Us</h2>
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="color-primary wow fadeInUp animation-delay-2">Description</h3>
                    <p class="wow fadeInUp animation-delay-2">
                        We are a team of excellent developers and testers who work with true passion to bring high quality
                        enterprise applications to you.
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        We work closely with our clients to understand their business needs and develop customized technology
                        solutions to meet those needs.
                    </p>

                </div>
                <div class="col-lg-6">
                    <h3 class="color-primary wow fadeInUp animation-delay-2">Skills & Expertise</h3>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> MVC, Spring Boot, Spring Quartz, Hibenate, Tiles, JPA).
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> .NET(.NET Core, Web form, Win form, MVC, WPF, API)
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> Web template design with Bootstrap, Foundation, HTML5 Boilerplate, Material
                        Designs.
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> Php (Laravel, CakePhp, Wordpress, Zoomla)
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> AngularJs, NodeJs, ReactJs, Ionic Framework.
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> Android, iOS
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> Database: SQL Server, MySQL, DB2, Postgres, NoSQL, Oracle.
                    </p>
                    <p class="wow fadeInUp animation-delay-2">
                        <i class="fa fa-check"></i> Automation Test with Selenium, Appium, SOASTA, Jenkins, ZapTest
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="team">
        <div class="container pt-6">
            <h1 class="color-primary text-center wow fadeInUp animation-delay-2" style="display: none;">Our Team</h1>
            <img src="assets/img/team.png" />
        </div>
    </section>
@endsection
