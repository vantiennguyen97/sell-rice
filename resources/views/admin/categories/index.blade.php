@extends('admin.layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Category List</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li style="margin-right: 10px">
                            <a href="{{ route('admin.importCategory') }}" class="btn btn-info">
                                <i class="fa-regular fa-file-excel"></i>
                                Import
                            </a>
                        </li>
                        <li style="margin-right: 10px">
                            <a href="{{ route('admin.newCategory') }}" class="btn btn-success">
                                <i class="fa-solid fa-plus"></i>
                                Add
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.listCategory') }}" class="btn btn-warning">
                                <i class="fa-solid fa-rotate-right"></i>
                                Reset
                            </a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <form action="{{ route('admin.deleteAllRecord') }}" method="POST" onsubmit="myFunction()" class="delete-form"
            style="margin-left: 22px; margin-bottom: 22px">
            @csrf
            <button type="button"class="btn btn-block btn-danger" id="delete-button">Delete All</button>
        </form>
    </div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <p>
                                @if (session('message'))
                                    <div class="alert alert-success mt-3">
                                        {{ session('message') }}
                                    </div>
                                @endIf
                            </p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div></div>
                                <a href="{{ route('admin.exportCategory') }}" class="btn btn-app bg-success">Export
                                    <i class="fas fa-inbox"></i>
                                </a>


                            </div>
                            <div class="dataTables_wrapper dt-bootstrap4" id="example1_wrapper">
                                {{-- <div class="row">
                                    <div class="col-sm-12 col-md-6"></div>
                                    <div class="col-sm-12 col-md-6"></div>
                                </div> --}}
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1" class="table table-bordered table-hover dataTable dtr-inline"
                                            aria-describedby="example1_info">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input type="checkbox">
                                                    </th>
                                                    <th>STT</th>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th>Font text</th>
                                                    <th>Font size</th>
                                                    <th>Font weight</th>
                                                    <th>Font style</th>
                                                    <th>Image</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($listCategory as $c)
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox">
                                                        </td>
                                                        <td>{{ $c->id }}</td>
                                                        <td>{{ $c->name }}</td>
                                                        <td>{{ $c->description }}</td>
                                                        <td>{{ $c->font_text }}</td>
                                                        <td>{{ $c->font_size }}</td>
                                                        <td>{{ $c->font_weight }}</td>
                                                        <td>{{ $c->font_style }}</td>
                                                        <td>
                                                            <img src="{{ asset('categories/resize/' . $c->image) }}"
                                                                alt="#" style="width: 100px; height: 100px;">
                                                        </td>
                                                        <td>
                                                            <button type="button" class="btn btn-info">
                                                                <i class="fas fa-edit"></i>
                                                                <a href="{{ route('admin.editCategory', $c->id) }}"
                                                                    style="color: aliceblue">Edit</a>
                                                            </button>
                                                            <form action="{{ route('admin.deleteCategory', $c->id) }}"
                                                                method="POST" onsubmit="myFunction()" class="delete-form">
                                                                @csrf

                                                                <button type="button"class="btn btn-danger"
                                                                    id="delete-button">
                                                                    <i class="fa-solid fa-trash"></i>
                                                                    Delete
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                            {{-- <tfoot>
                                                <tr>

                                                </tr>
                                            </tfoot> --}}
                                    </div>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script>
        document.querySelectorAll('#delete-button').forEach(button => {
            button.addEventListener('click', function(event) {
                event.preventDefault();
                const form = this.closest('.delete-form');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        form.submit();
                    }
                })
            });
        });
    </script>
@endsection
