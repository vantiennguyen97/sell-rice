@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create Category</h3>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{ route('admin.storeCategory') }}" method="POST" enctype="multipart/form-data" id="categoryForm">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">Name Category</label>
                        <input type="text" class="form-control" id="name" name="name"
                            onclick="checkNameValidate()">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea id="description" name="description"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="font-text">Font Text

                                    <select name="font_text" id="font-text" class="form-control">
                                        @foreach ($font_text as $ft)
                                            @foreach ($ft as $key => $value)
                                                <option value="{{ ucfirst($key) }}">{{ $value }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>

                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="font-size">Font Size
                                    <input type="number" name="font_size" id="font-size" class="form-control">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="font-weight">Font weight
                                    <select name="font_weight" id="font-weight" class="form-control">
                                        @foreach ($font_weight as $fw)
                                            @foreach ($fw as $key => $value)
                                                <option value="{{ ucfirst($key) }}">{{ $value }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="font-style">Font Style
                                    <select name="font_style" id="font-style" class="form-control">
                                        @foreach ($font_style as $fst)
                                            @foreach ($fst as $key => $value)
                                                <option value="{{ ucfirst($key) }}">{{ $value }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                            <div class="input-group-append">
                                <span class="input-group-text">Upload</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
    {{-- text-area --}}
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed linkchecker a11ychecker tinymcespellchecker permanentpen powerpaste advtable advcode editimage advtemplate ai mentions tinycomments tableofcontents footnotes mergetags autocorrect typography inlinecss markdown',
            toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | addcomment showcomments | spellcheckdialog a11ycheck typography | align lineheight | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
            mergetags_list: [{
                    value: 'First.Name',
                    title: 'First Name'
                },
                {
                    value: 'Email',
                    title: 'Email'
                },
            ],
            ai_request: (request, respondWith) => respondWith.string(() => Promise.reject(
                "See docs to implement AI Assistant")),
        });
    </script>
    {{-- validation --}}
    <script>
        function checkNameValidate() {

            // Name validation
            const name = document.getElementById("name")

            if (name == null) {
                name.classList.add('is-invalid');
            }else{
                name.classList.add('is-valid');
            }
        }
    </script>
@endsection
