/**
 * iSoVietProfile
 */

var iSoVietProfile = {
    Project: {
        items: function () {
            return [
                {
                    title: "Altos Partners LLC",
                    thumbnail: "assets/img/demo/port23.jpg",
                    description: "Altos Energy Partners is a Houston-based private equity firm that creatively invests in energy-related service and manufacturing companies, maximizing the value of its portfolio through patient, long-term capital and its operating general partner resources."
                }, 
                {
                    title: "Data Collection Platform",
                    thumbnail: "assets/img/demo/port4.jpg",
                    description: `
                        <p>
                            SD Analytic is a collection data platform that helps you scrape data from web sites into databases or CSV file and will analyze the data according to your business.
                        </p>
                        <p>
                            Our Smart Data Analytics (SD Analytic) is the ultimate tool for traders around the world about any fields. SD Analytic help us understand the markets and help us soon discover the huge potential and useful information so that we can have the right decision in our business. SD Analytic can help you with the following:
                        </p>`
                },
                {
                    title: "EIG Partners",
                    thumbnail: "assets/img/demo/port7.jpg",
                    description: `EIG is an independent partnership with a distinct culture focused on teamwork, integrity and transparency. We operate as a single investment team and all of our senior professionals have capital committed to our funds.`
                }
            ]
        },
        init: function () {
            const items = iSoVietProfile.Project.items();
            var html = "";
            $.each(items, function (index, item) {
                html += iSoVietProfile.Project.showItem(index, item);
            });
            $(".portfolio-items").html(html);
        },
        showItem: function (index,item) {
            var html = `<div class="col-lg-4 col-md-6 col-sm-6 mb-4">
                <div class="ms-thumbnail-container wow fadeInUp">
                <figure class="ms-thumbnail ms-thumbnail-top ms-thumbnail-info">
                    <img src="${item.thumbnail}" alt="" class="img-fluid">
                    <figcaption class="ms-thumbnail-caption text-center">
                    <div class="ms-thumbnail-caption-content">
                        <h3 class="ms-thumbnail-caption-title">${item.title}</h3>
                        <a href="javascript:void(0)" onclick="iSoVietProfile.Project.details(${index});" class="btn btn-raised btn-danger" data-toggle="modal" data-target="#projectModal"><i class="zmdi zmdi-eye"></i> View more</a>
                    </div>
                    </figcaption>
                </figure>
                </div>
            </div>`;
            return html;
        },
        details: function(index) {
            const items = iSoVietProfile.Project.items();
            const item = items[index];
            var modalTitleObj = $('#projectModal .modal-title');
            var modalBodyObj = $('#projectModal .modal-body');
            modalTitleObj.html(item.title);
            modalBodyObj.html($.trim(item.description));
        }
    }
};

$(document).ready(function () {
    console.log("Init");
    iSoVietProfile.Project.init();
});