<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
return [
    'is_debug' => 1,
    'main_language' => 'de',
    'languages' => array(
        'en',
//        'vi',
        'de'
    ),
    'status' => array(
        '1' => 'Enabled',
        '0' => 'Disabled'
    ),
    'menu_type' => array(
        'header' => 'Header',
        'footer' => 'Footer'
    ),
    'size_small_image_food' => [
        'height' => 180,
        'width' => 200
    ],
    'menu_download_image' => [
        'height' => 386,
        'width' => 317
    ],
    'size_small_avatar' => [
        'height' => 180,
        'width' => 200
    ],
    'size_small_logo' => [
        'height' => 108,
        'width' => 370
    ],
    'size_small_favicon' => [
        'height' => 32,
        'width' => 32
    ],
    'size_small_image_newspaper' => [
        'height' => 180,
        'width' => 200
    ],
    'size_small_image_article' => [
        'height' => 180,
        'width' => 200
    ],
    'size_small_image_recipe' => [
        'height' => 180,
        'width' => 200
    ],
    'size_small_icon_link' => [
        'height' => 40,
        'width' => 40
    ],
    'gender' => array(
        '1' => 'Male',
        '0' => 'Female'
    ),
    'main_food_type_id' => 1,
    'password_default' => 'camly@password',
    'setting_key_arr' => [
        'name',
        'address',
        'opening_hours',
        'copyright',
        'link_facebook',
        'link_twitter',
        'link_logo',
        'link_favicon',
        'email',
        'phone'
    ],
    'target' => array(
        '1' => 'Self',
        '0' => 'Blank'
    ),
    'order_status' => array(
        '0' => 'Pending',
        '1' => 'Completed',
        '2' => 'Cancel'
    ),
    'delete_status' => array(
        '0' => 'All',
        '1' => 'Soft Delete'
    ),
    'finance_type' => array(
        '0' => 'Days',
        '1' => 'Months',
        '2' => 'Years'
    ),
    'category' => array(
        'aboutus' => 'About Us',
        'contact' => 'Contact',
        'other' => 'Other'
    ),
    'link_category' => array(
        'social' => 'Social',
        'other' => 'Other'
    ),
    'day_of_week' => array(
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
        0 => 'Sunday',
    ),
    'font_text' => array (
        'Arial' => 'Arial',
        'Times New Roman' => 'Times New Roman',
        'Helvetica' => 'Helvetica',
        'Times' => 'Times',
        'Courier New' => 'Courier New',
        'Verdana' => 'Verdana',
        'Courier' => 'Courier',
        'Arial Narrow' => 'Arial Narrow',
        'Candara' => 'Candara',
        'Geneva' => 'Geneva',
        'Calibri' => 'Calibri',
        'Optima' => 'Optima',
        'Cambria' => 'Cambria',
        'Garamond' => 'Garamond',
        'Perpetua' => 'Perpetua',
        'Monaco' => 'Monaco',
        'Didot' => 'Didot',
        'Brush Script MT' => 'Brush Script MT',
        'Lucida Bright' => 'Lucida Bright',
        'Copperplate' => 'Copperplate'
    ),
    'font_weight' => [
        'Normal' => 'Normal',
        'Bold' => 'Bold',
    ],
    'font_style' => [
        'Normal' => 'Normal',
        'Italic' => 'Italic',
        'Oblique' => 'Oblique',
    ],
    'payment_type' => [
        '' => '---None---',
        'percent' => 'Percent',
        'money' => 'Money'
    ],
    'payment_method' => [
        'paypal' => 'Paypal',
        'sofort' => 'Sofort',
        'creditcard' => 'Creditcard',
        'banktransfer' => 'BankTransfer'
    ],
    'payment_waiting_time' => env('PAYMENT_WAITING_TIME', 15000), //microsecond
    'maximum_income' => env('MAXIMUM_INCOME', 999999999999)
];

